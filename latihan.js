// Nomor 1 (Arrow Function)
const golden = () => {console.log("this is golden!!!");};

golden();


// Nomor 2 (Object Literal)
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function() {
      console.log(firstName + " " + lastName);
      return;
    }
  };
};

newFunction("William", "Imoh").fullName();


// Nomor 3 (Destructuring)
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
};

const {firstName, lastName, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation);


// Nomor 4 (Array Spreading)
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];

console.log(combined);

// Nomor 5 (Template Literals)
const planet = "earth";
const view = "glass";

let before = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before); 
